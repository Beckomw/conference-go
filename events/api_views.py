from django.http import JsonResponse, Http404
from common.json import ModelEncoder, DateEncoder
from .models import Conference, Location

class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "state",
    ]

    def get_extra_data(self, o):
        return {"href": o.get_api_url()}

class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]

    encoders = {
        "location": LocationDetailEncoder(),
        
    }

def api_list_conferences(request):
    """
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """
    response = []
    conferences = Conference.objects.all()
    for conference in conferences:
        response.append(
            {
                "name": conference.name,
                "href": conference.get_api_url(),
            }
        )
    return JsonResponse({"conferences": response})

def api_show_conference(request, id):
    """
    Returns the details for the Conference model specified
    by the id parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.

    {
        "name": the conference's name,
        "starts": the date/time when the conference starts,
        "ends": the date/time when the conference ends,
        "description": the description of the conference,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "max_presentations": the maximum number of presentations,
        "max_attendees": the maximum number of attendees,
        "location": {
            "name": the name of the location,
            "href": the URL for the location,
        }
    }
    """
    try:
        conference = Conference.objects.get(id=id)
    except Conference.DoesNotExist:
        raise Http404("Conference not found")

    return JsonResponse(
        conference,
        encoder=ConferenceDetailEncoder,
        safe=False
    )

def api_list_locations(request):
    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
    """
    locations = []
    conferences = Conference.objects.all()
    for conference in conferences:
        locations.append({
            "name": conference.location.name,
            "href": conference.location.get_api_url(),
        })
    return JsonResponse({"locations": locations})

def api_show_location(request, id):
    """
    Returns the details for the Location model specified
    by the id parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """
    try:
        location = Location.objects.get(id=id)
    except Location.DoesNotExist:
        raise Http404("Location not found")

    location_data = {
        "name": location.name,
        "city": location.city,
        "room_count": location.room_count,
        "created": location.created.isoformat(),
        "updated": location.updated.isoformat(),
        "state": str(location.state),
        "href": location.get_api_url(),
    }
    return JsonResponse(location_data)
